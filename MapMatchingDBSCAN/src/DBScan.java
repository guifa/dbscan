


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class DBScan {

	public static DBManager db = null;
	public static KdTree<KdTree.XYZPoint> kdTree = null;
	public ArrayList<KdTree.XYZPoint> points = null;
	public ArrayList<KdTree.XYZPoint> pointsKdTree = null;

	protected static final int REGION_QUERY = 0;
	protected static final int KDTREE_RECURSIVE = 1;

	public DBScan(String ip, String user, String passwd) {
		db = new DBManager(ip, user, passwd);
	}

	public static double latitudeY(double latitude) {
		return 6378137*java.lang.Math.log(java.lang.Math.tan(java.lang.Math.PI / 4 + 0.5
				* java.lang.Math.toRadians(latitude)));
	}

	public static double longitudeX(double longitude){
		return 6378137 * java.lang.Math.toRadians(longitude);
	}
	
	public static double metersYtoLatitude(double Y){
		return java.lang.Math.toDegrees(java.lang.Math.atan(Y/6378137));
	}
	
	public static double meterXtoLongitude(double X){
		return java.lang.Math.toDegrees(X/6378137);
	}
	

	//converte os pontos recebidos em lat,long em coordenadas x,y. Carrega os pontos em uma estrutura
	//de indice
	@SuppressWarnings("resource")
	public ArrayList<KdTree.XYZPoint> loadPoints(String f){
		String l;
		String[] line;		
		Double pX, pY;
		KdTree.XYZPoint p;
		points = new ArrayList<KdTree.XYZPoint>();
		BufferedReader br;
		int index = 0;

		try {
			br = new BufferedReader(new FileReader(f));
			while((l = br.readLine())!=null){	

				line = l.split(",");
				pY = latitudeY(Double.parseDouble(line[0]));
				pX = longitudeX(Double.parseDouble(line[1])); 
				
//				pY = Double.parseDouble(line[1]);
//				pX = Double.parseDouble(line[0]); 
				
				if(!pY.isNaN() && !pX.isNaN()){
					p = new KdTree.XYZPoint(pY,pX);
					p.setIndex(index);
					points.add(p);
					index++;
				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		pointsKdTree = new ArrayList<KdTree.XYZPoint>(points);
		return points;
	}

	public KdTree<KdTree.XYZPoint> createIndex(){
		kdTree = new KdTree<KdTree.XYZPoint>(pointsKdTree);
		System.out.println(KdTree.TreePrinter.getString(kdTree));
		return kdTree;
	}

	//executa o algoritmo de dbscan
	public void dbscan(String key, double eps, int minPoints, int method){

		try {
			int pos, cluster = 0;
			Collection<Integer> neighborPts = null;

			ArrayList<String> id_clusters;

			while((pos = getUnvisitedPoint(points)) != -1){
				points.get(pos).setIsvisited(true);

				if (method==KDTREE_RECURSIVE) {
					neighborPts = regionQueryKdTreeRecursive(pos, points, eps);
				} else {
					neighborPts = regionQuery(pos, points, eps);					
				}
				
//				System.out.println();
				
				if(neighborPts.size() < minPoints){
					points.get(pos).setNoise(true);
				} else {
					cluster++;
					points.get(pos).setCluster(key+cluster);
					points.get(pos).setIscore(true);
					expandCluster(neighborPts, cluster, eps, minPoints, key, method);
				}
			}

			//insere os pontos dos clusters no banco de dados
		id_clusters = db.insertClusters(points);
		db.createClustersGeometry(id_clusters);

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}

	}	

	//expande o cluster por meio do indice, colocando os vizinhos como pontos do cluster 
	private void expandCluster(Collection<Integer> neighborPts, int cluster, double eps, int minPoints, String key, int method){
		Collection<Integer> neighborPts2;    	  
		KdTree.XYZPoint p;
		int index = 0;

		ArrayList<Integer> listNeigh = new ArrayList<Integer>();
		listNeigh.addAll(neighborPts);

		for (int j = 0; j < listNeigh.size(); j++) {
			index = listNeigh.get(j);
			
			p = points.get(index);

			if(!p.isVisited()){
				points.get(index).setIsvisited(true);

				if (method==KDTREE_RECURSIVE) {
					neighborPts2 = regionQueryKdTreeRecursive(index, points, eps);
				} else {
					neighborPts2 = regionQuery(index, points, eps);					
				}
								
				if(neighborPts2.size() >= minPoints) {
					points.get(index).setIscore(true);
					listNeigh.addAll(neighborPts2);
				}
			}

			if(points.get(index).getCluster().equals("")) {
				points.get(index).setCluster(key+cluster); 			
			}
		}
	}

	//retorna os pontos vizinhos ao ponto que esta na posicao "pos"
	private static Collection<Integer> regionQuery(int pos, ArrayList<KdTree.XYZPoint> points, double eps) {

		KdTree.XYZPoint p = points.get(pos);
		Set<Integer> neighborPts = new HashSet<Integer>();

		for (int i = 0; i < points.size(); i++) {
			if (getDistance(p.getLon(), points.get(i).getLon(), p.getLat(), points.get(i).getLat()) <= eps) {
				neighborPts.add(i);				
			}
		}

		points.get(pos).setNeigh(neighborPts.size());

		return neighborPts;
	}

	//retorna os pontos vizinhos ao ponto que esta na posicao "pos", baseado no indice
	private static Collection<Integer> regionQueryKdTreeRecursive(int pos, ArrayList<KdTree.XYZPoint> points, double eps) {
		Collection<Integer> neighborPts = new HashSet<Integer>();

		neighborPts = kdTree.rangeRecursive(points.get(pos), eps);

		points.get(pos).setNeigh(neighborPts.size());
		return neighborPts;
	}

	//retorna o primeiro ponto ainda nao vizitado
	private static int getUnvisitedPoint(ArrayList<KdTree.XYZPoint> points) {

		for (int i = 0; i < points.size(); i++) {
			if(!points.get(i).isVisited()){
				return points.get(i).getIndex();
			}			
		}			

		return -1;
	}

	//calcula distancia entre dois pontos representados em coordenadas geograficas
	public static double getDistance(double x1, double x2, double y1, double y2) {			
		return Math.sqrt(Math.pow(x1-x2, 2)+ Math.pow(y1-y2, 2));
	}

	//verifica se dois clusters podem sofrer merge, por terem estado em particoes diferentes
	public boolean mergeDbscan(String id_cluster1, String id_cluster2, double eps, int minPoints){
		boolean canMerge=false;
		try {

			ArrayList<KdTree.XYZPoint> points1 = new ArrayList<KdTree.XYZPoint>(), points2 = new ArrayList<KdTree.XYZPoint>();
			String[] line;
			KdTree.XYZPoint p1,p2;
			String str;

			//os pontos sao salvos em arquivo correspondente ao cluster ao qual pertencem, caso nao haja o arquivo, o ponto
			//eh retirado do banco
			if(new File("/home/victor/dbscan/"+id_cluster1+"points.txt").exists()){
				try {
					BufferedReader in = new BufferedReader(new FileReader("/usr/local/hadoop/hadoop-1.1.2/streets/"+id_cluster1+"points.txt"));
					while (in.ready()) {
						str = in.readLine();
						line = str.split(",");
						p1 = new KdTree.XYZPoint(Double.parseDouble(line[0]), Double.parseDouble(line[1]), Boolean.parseBoolean(line[2]), 
								Integer.parseInt(line[3]), id_cluster1, Boolean.parseBoolean(line[4]));
						points1.add(p1);
					}
					in.close();
				} catch (IOException e) {
					e.printStackTrace(); 
				}
			}
			else points1 = db.getClusterPoints(id_cluster1);
			System.out.println(points1.size());

			if(new File("/home/victor/dbscan/"+id_cluster2+"points.txt").exists()){
				try {
					BufferedReader in = new BufferedReader(new FileReader("/home/victor/dbscan/"+id_cluster2+"points.txt"));
					while (in.ready()) {
						str = in.readLine();
						line = str.split(",");
						p2 = new KdTree.XYZPoint(Double.parseDouble(line[0]), Double.parseDouble(line[1]), Boolean.parseBoolean(line[2]), 
								Integer.parseInt(line[3]), id_cluster1, Boolean.parseBoolean(line[4]));
						points2.add(p2);
					}
					in.close();
				} catch (IOException e) {
					e.printStackTrace(); 
				}
			}
			else points2 = db.getClusterPoints(id_cluster2);
			System.out.println(points2.size());


			//matriz para identificar quando dois pontos de clusters distintos sao vizinhos
			boolean[][] adj = new boolean[points1.size()][points2.size()];

			//verifica qual a distancia de dois pontos de clusters distintos, caso seja menor que eps (sejam vizinhos),
			//incrementa o neigh dos pontos
			for (int i = 0; i < points1.size(); i++) {
				p1 = points1.get(i);
				for (int j = 0; j < points2.size(); j++) {
					p2 = points2.get(j);
					if(getDistance(p1.getLon(),p2.getLon(),p1.getLat(),p2.getLat())<=eps){
						points1.get(i).increaseNeigh();
						points2.get(j).increaseNeigh();
						adj[i][j]=true;
					}
				}
				//novos vizinhos permitiram que virasse core point no cluster 1
				if(p1.getNeigh()>=minPoints) points1.get(i).setIscore(true);
			}

			//novos vizinhos permitiram que ponto virasse core point no cluster 2
			for (int j = 0; j < points2.size(); j++) {
				p2 = points2.get(j);
				if(p2.getNeigh()>=minPoints) points2.get(j).setIscore(true);
			}

			//verifica se os clusters podem sofrer merge, ou seja, se existem dois pontos que sao cores que sao vizinhos
			for (int i = 0; i < points1.size(); i++) {
				p1 = points1.get(i);
				for (int j = 0; j < points2.size(); j++) {
					p2 = points2.get(j);
					if(adj[i][j] && p2.isCore() && p1.isCore()){
						canMerge=true;
						//se p2 ou p1 forem noise, com o merge eles deixam de ser
						if(p1.isNoise()) points1.get(i).setNoise(false);
						if(p2.isNoise()) points2.get(j).setNoise(false);
					}
					else if(adj[i][j] && p2.isCore() && p1.isNoise()){
						points1.get(i).setNoise(false);
					}
					else if(adj[i][j] && p1.isCore() && p2.isNoise()){
						points2.get(j).setNoise(false);			
					}
				}				
			}
			if(canMerge){
				//devolve os valores atualizados pro banco ou pra aplicacao que chamou esse merge
								
				BufferedWriter out = new BufferedWriter(new FileWriter("/home/victor/dbscan/"+id_cluster1+"points.txt",false));	
				points1.addAll(points2);				
				//db.updatePoints(id_cluster1,points1);
				for (int i = 0; i < points1.size(); i++) {
					//lat, lon, iscore, #neigh, cluster, isnoise
					p1 = points1.get(i);
					out.write(p1.getLat()+","+p1.getLon()+","+p1.isCore()+","+p1.getNeigh()+","+id_cluster1+","+p1.isNoise()+"\n");

				}
				out.close();		
			}			
		} catch (SQLException e) {
			e.printStackTrace();
		}// catch (IOException e) {
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//	e.printStackTrace();
		//}
		return canMerge;
	}

	//chama procedure do banco que verifica os candidatos a merge
	public void findIntersectionClusters(double eps) throws SQLException{	
		db.findIntersectionClusters(eps);
	}

	//chama a procedure do banco que controi um arquivo dos candidatos a merge
	public void clustersToMerge() throws SQLException, IOException{
		db.clustersToMerge();
	}
}
