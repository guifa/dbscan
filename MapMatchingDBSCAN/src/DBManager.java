



import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class DBManager{
	private static Connection c;
	private static PreparedStatement stmt = null;
	private static Statement stm = null;
	private static ResultSet rs;
	
	public DBManager(String ip, String user, String passwd) {
		c = new ConnectionFactory().getConnection(ip,user,passwd);
	}
	
	
	//insere no banco de dados os pontos de cada cluster encontrado no DBScan 
	public ArrayList<String> insertClusters(ArrayList<KdTree.XYZPoint> points) throws SQLException{
		KdTree.XYZPoint p;
		ArrayList<String> id_clusters = new ArrayList<String>();
		stm = c.createStatement();
	     for (int j = 0; j < points.size(); j++) {
	    	 p = points.get(j);
	    	 stm.executeUpdate("insert into points values('"+p.getCluster()+"',"+DBScan.metersYtoLatitude(p.getLat())+","+
	    	 			DBScan.meterXtoLongitude(p.getLon())+","+p.isCore()+",ST_SetSRID(ST_MakePoint("+DBScan.meterXtoLongitude(p.getLon())+","+DBScan.metersYtoLatitude(p.getLat())+"),4326),"+p.getNeigh()+","+p.isNoise()+")");
	    	 if(!id_clusters.contains(p.getCluster()) && !p.isNoise()) id_clusters.add(p.getCluster());
	     }
	    stm.close();	
		return id_clusters;
	}
		
	//recupera os pontos de um cluster dado de entrada
	@SuppressWarnings("static-access")
	public ArrayList<KdTree.XYZPoint> getClusterPoints(String id_cluster) throws NumberFormatException, SQLException{
		KdTree.XYZPoint p1;
		String sql = "select lat, lon, iscore, neigh, isnoise from points where cluster='"+id_cluster+"'";
		stmt = c.prepareStatement(sql, rs.TYPE_SCROLL_INSENSITIVE, rs.CONCUR_READ_ONLY); 		
		rs = stmt.executeQuery();
		
		ArrayList<KdTree.XYZPoint> points = new ArrayList<KdTree.XYZPoint>();
		while(rs.next()){
			p1 = new KdTree.XYZPoint(Double.parseDouble(rs.getString(1)), Double.parseDouble(rs.getString(2)), Boolean.parseBoolean(rs.getString(3)), 
					Integer.parseInt(rs.getString(4)), id_cluster, Boolean.parseBoolean(rs.getString(5)));
			points.add(p1);
		}
		stmt.close();
	 	rs.close();
		return points;
	}	
	
	//cria as geometrias no banco de dados dos clusters dados de entrada por meio de um procedimento armazenado
	public void createClustersGeometry(ArrayList<String> id_clusters){
		try {
			String sql = "select createClustersGeometry(?)";
			stmt = c.prepareStatement(sql);
			for (int j = 0; j < id_clusters.size(); j++) {
				 stmt.setString(1, id_clusters.get(j));
				 stmt.executeQuery();
			}
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


	//modifica o conteudo dos pontos do cluster dado de entrada. Eh chamado apos a ocorrencia do merge de clusters.
	public void updatePoints(String id_cluster1, ArrayList<KdTree.XYZPoint> points) throws SQLException {
		KdTree.XYZPoint p;
		stm = c.createStatement();
	     for (int j = 0; j < points.size(); j++) {
	    	 p = points.get(j);
	    	 //cluster, lat, long, isCore, geometry, #neig, isNoise
	    	 stm.executeUpdate("update points set cluster='"+id_cluster1+"',iscore="+p.isCore()+",neigh="+p.getNeigh()+",isnoise="+p.isNoise()+
	    	 		" where cluster='"+p.getCluster()+"' and lat="+p.getLat()+" and lon="+p.getLon());
	     }
	     stm.close();	
	}
	
	//encontra os clusters que sao candidatos a merge
	public void findIntersectionClusters(double eps) throws SQLException{
		String sql = "select intersectionCluster(?)";
		stmt = c.prepareStatement(sql);
		stmt.setDouble(1, eps);
	 	stmt.executeQuery();
	 	stmt.close();
	}

	//guarda no arquivo MergeClusters.txt quais os candidatos a merge
	@SuppressWarnings("static-access")
	public void clustersToMerge() throws SQLException, IOException {
		File file = new File("/usr/local/hadoop/hadoop-1.1.2/MergeClusters.txt"); 
		String sql = "select distinct(id_clustermain, id_cluster1, id_cluster2) from tableTouches";
		stmt = c.prepareStatement(sql, rs.TYPE_SCROLL_INSENSITIVE, rs.CONCUR_READ_ONLY);
	 	rs = stmt.executeQuery(); 
	 	
	 	//este arquivo eh entrada para o segundo map reduce
	 	BufferedWriter out = new BufferedWriter(new FileWriter(file,true));	
	 	
	 	while(rs.next()){
	 		out.write(rs.getString(1)+"\n"); 
	 	}
	 	out.close();
	 	stmt.close();
	 	rs.close();
	}	
}
