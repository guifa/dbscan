import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.sql.SQLException;


public class FileManager {
	
	public void mergeTrajectoryFiles(String newFile, String directory) throws SQLException, IOException {
		String l;
		String[] line;	
		String pX, pY;
		BufferedReader br;
		File allTrajectories = new File(newFile); 
		
		FilenameFilter filter = new FilenameFilter() {
		    public boolean accept(File dir, String name) {
		        return name.endsWith(".txt");
		    }
		};

		File folder = new File(directory);
		File[] listOfFiles = folder.listFiles(filter);
		BufferedWriter out = new BufferedWriter(new FileWriter(allTrajectories,true));	
		for (int i = 0; i < listOfFiles.length; i++) {
		    File file = listOfFiles[i];
		    try {
				br = new BufferedReader(new FileReader(file));
				while((l = br.readLine())!=null){	
					line = l.split(";");					
					pY = line[3];
					pX = line[4];
					out.append(pY + "," + pX + "\n");
				}

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}	    
		}
	 	out.close();
	}

}
