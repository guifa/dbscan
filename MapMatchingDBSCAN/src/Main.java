import java.io.IOException;
import java.sql.SQLException;


public class Main {

	private static double eps;
	private static int minPoints;
	private static String ip, user, passwd;
	
	public static void main(String[] args) throws IOException, SQLException {
			
	    	eps = Double.parseDouble(args[0]);
	    	minPoints = Integer.parseInt(args[1]);
	    	ip = args[2];
	    	user = args[3];
	    	passwd = args[4];
		
	    	DBScan dbscan = new DBScan(ip,user,passwd);		   
		    dbscan.loadPoints(args[5]);
		    dbscan.createIndex();			    
		    dbscan.dbscan("",eps, minPoints, 1); //MUDEI PARA LAT,LONG
	    
//		    FileManager fileManager = new FileManager();
//		    fileManager.mergeTrajectoryFiles("C:\\Users\\Guif�\\workspace\\all_trajectories.txt", "C:\\Users\\Guif�\\Trajetorias");

		   	 		     
		}
	
}

