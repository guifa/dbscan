




import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
    
	public Connection getConnection(String ip, String user, String passwd) {
        try {
        	try {
				Class.forName("org.postgresql.Driver");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
        	//return DriverManager.getConnection("jdbc:postgresql://10.102.12.140:5432/postgres?autoReconnect=true","postgres","cloudufc");
            return DriverManager.getConnection("jdbc:postgresql://"+ip+":5432/postgres?autoReconnect=true",user,passwd);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
	
}